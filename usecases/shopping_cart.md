# Shopping Cart Use Case

### Scenario: Buyer adds products into the shopping cart
```gherkin
Given buyer is at product page
When buyer selected product choice
And buyer input quantity
And buyer click Add To Cart button
Then product and quantity should be added shopping cart
```

### Scenario: Buyer views shopping cart
```gherkin
Given some products are in shopping cart
When buyer click shopping cart button
Then the shopping cart with products and quantity will be shown up.
```

### Scenario: Buyer modify product quantity in shopping cart
```gherkin
Given some products are in shopping cart
When buyer modify a product quantity
Then the product quantity modified in database
```

### Scenario: Buyer delete products in shopping cart
```gherkin
Given some products are in shopping cart
When buyer delete some products
Then the products will be removed from database
```

# API
### POST /
```json
{
    "products": [
        {
            "productId": "wer324",
            "quantity": 3,
        },
        {
            "productId": "asdf434",
            "quantity": 2,
        }
    ]
}
```

### Response
> Current shopping cart items for the buyer
```json
{
    "shoppingCart": {
        "buyerId": "asdf12123",
        "products" : [
            {
                "productId": "asf123",
                "quantity": 1,
            },
            {
                "productId": "aff456",
                "quantity": 3,
            }
        ]
    }
}
```

### GET /
### Response
```json
{
    "shoppingCart": {
        "buyerId": "asdf12123",
        "products" : [
            {
                "productId": "asf123",
                "quantity": 1,
            },
            {
                "productId": "aff456",
                "quantity": 3,
            }
        ]
    }
}
```

### PATCH /
```json
{
    "products": [
        {
            "productID": "asdf324",
            "quantity": 3,
        },
        {
            "productID": "ads458",
            "quantity": 2,
        }
    ]
}
```

### Response
> Current shopping cart items for the buyer
```json
{
    "shoppingCart": {
        "buyerId": "asdf12123",
        "products" : [
            {
                "productId": "asf123",
                "quantity": 1,
            },
            {
                "productId": "aff456",
                "quantity": 3,
            }
        ]
    }
}
```

### DELETE /?id=asdfw3123123&id=asdfw31231asd
### Response
> Current shopping cart items for the buyer
```json
{
    "shoppingCart": {
        "buyerId": "asdf12123",
        "products" : [
            {
                "productId": "asf123",
                "quantity": 1,
            },
            {
                "productId": "aff456",
                "quantity": 3,
            }
        ]
    }
}
```