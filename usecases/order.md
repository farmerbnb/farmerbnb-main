# Order Use Case

### Scenario: Buyer create an order
```gherkin
Given buyer is at shopping cart page
And shipping cart is not empty
When buyer click Order button
Then the shipping page appears
When buyer click one of the shipping method
And buyer click Go Payment button
Then the payment page appears
When buyer choose the payment method and fill in the info
And buyer click Pay button
Then order created page will be shown up
```

### Scenario: Buyer view order history
```gherkin
Given buyer has created order before
When buyer click My Order button
Then a list of orders appear
And each order has created date/total price/order no shown
And echo order has a list of product/quantity/price/picture shown
And echo order has delivery status shown
```

### Scenario: Buyer view an order
```gherkin
Given buyer is at order history page
When buyer click one of the orders
Then the order detail page shown up
And order detail has created date/total price/order no shown
And order deital has a list of product/quantity/price/picture shown
And order has delivery address/company/fee/status shown
```

### Scenario: Seller view order history
```gherkin
Given seller has some orders
When seller click My Order button
Then a list of orders appear
And each order has created date/toal price/order no/buyer name shown
And each order has a list of product/quantity/price/picture belong to this seller shown
And echo order has delivery status shown
```

### Scenario: Seller view an order
```gherkin
Given seller is at order history page
When seller click one of the orders
Then the order details page shown up
And order detail has created date/total price/order no/buyer name shown
And order deital has a list of product/quantity/price/picture belong to this seller shown
And order has delivery address/company/fee/status shown
```


# API
### POST /
```json
{
    "products": [
        {
            "productId": "asd123",
            "quantity": 1,
            "sellerId": "sdf459",
            "totalPrice": 456
        },
        {
            "productId": "asd456",
            "quantity": 3,
            "sellerId": "qwer459",
            "totalPrice": 456
        }
    ],
    "deliveryId": "asfoiur123123",
}
```

### Response
```json
{
    "orderId": "adf123123",
    "date": 123123
}
```

### GET /?page=0&items=10
### Response
```json
{
    "orders": [
        {
            "products": [
                {
                    "productId": "asfwer123",
                    "quantity": 1,
                    "sellerId": "asfwer123",
                    "totalPrice": 456
                },
                {
                    "productId": "asfwer123",
                    "quantity": 3,
                    "sellerId": "asfwer123",
                    "totalPrice": 456
                }
            ],
            "date": 123123123,
            "buyerId": "erww456",
            "orderId": "asdf123",
            "deliveryId": "asdf123asdf",
        },
        {
            "products": [
                {
                    "productId": "asfwer123",
                    "quantity": 1,
                    "sellerId": "asfwer123",
                    "totalPrice": 456
                },
                {
                    "productId": "asfwer123",
                    "quantity": 3,
                    "sellerId": "asfwer123",
                    "totalPrice": 456
                }
            ],
            "date": 123123123,
            "buyerId": "asfwer123",
            "orderId": "qwe123",
            "deliveryId": "asdf123as12",
        }
    ],
}
```

### GET /?orderId=qwe123
### Response
```json
{
    "products": [
        {
            "productId": "qweas123as",
            "quantity": 1,
            "sellerId": "qweas123as",
            "totalPrice": 456
        },
        {
            "productId": "qweas123as",
            "quantity": 3,
            "sellerId": "qweas123as",
            "totalPrice": 456
        }
    ],
    "date": 123123123,
    "buyerId": "qweas123as",
    "orderId": "qwe123",
    "deliveryId": "asdf123as12",
}
```

### GET /?seller=true&page=0&items=10
### Response
```json
{
    "orders": [
        {
            "products": [
                {
                    "productId": "wrsf23s",
                    "quantity": 1,
                    "sellerId": "wrsf23s",
                    "totalPrice": 456
                },
                {
                    "productId": "wrsf23s",
                    "quantity": 3,
                    "sellerId": "wrsf23s",
                    "totalPrice": 456
                }
            ],
            "date": 123123123,
            "buyerId": "wrsf23s",
            "orderId": "asf144",
            "deliveryId": "asdf123as12",
        },
        {
            "products": [
                {
                    "productId": "asdwer123",
                    "quantity": 1,
                    "sellerId": "asdwer123",
                    "totalPrice": 456
                },
                {
                    "productId": "asdwer123",
                    "quantity": 3,
                    "sellerId": "asdwer123",
                    "totalPrice": 456
                }
            ],
            "date": 123123123,
            "buyerId": "wrqw12312",
            "orderId": "asf144",
            "deliveryId": "asdf123as12",
        }
    ],
}
```

### GET /?seller=true&orderId=asf144
### Response
```json
{
    "products": [
        {
            "productId": "qwer123123",
            "quantity": 1,
            "sellerId": "qwer123123",
            "totalPrice": 456
        },
        {
            "productId": "qwer123123",
            "quantity": 3,
            "sellerId": "qwer123123",
            "totalPrice": 456
        }
    ],
    "date": 123123123,
    "buyerId": "wrqw12312",
    "orderId": "asf144",
    "deliveryId": "asdf123as12",
}
```
