Given the customer has purchase a product from the platform

AND the customer has received the product

AND the customer on the 'order completed' page

WHEN the customer clicks on 'Write a review' button

THEN the system redirects the customer to  review page

AND once the customer clicks on 'submit' button

THEN the system displays message to confirm submission.

### API:

Get product details -> display product information

	GET/Product?id=123
	
	{
		"productId":123,
		"productPrice": 25,
		"productUnit": "kg"
	}
Get order details -> get related order that buyer had paid for
	
	GET/Orders?id=123
	
	{
		"BuyerId":123,
		"Items": [{"item1":"abc","item2":"cde"}],
	}
	
Get customer detail -> get buyer information

	GET/Buyer?id=123
	
	{
		"buyerId":123,
		"buyerName": 25,
		"productUnit": "kg"
	}

Get list of reviews of the product

GET/Reviews?productid=123 -> get list of reviews related to this product
	
	{
		"productId":123,
	}
	
Post review -> create product review

{
	"buyerId":123,
	"productId": 234,
	"reviewMsg": "highly recommended!",
	"rating": 4
}