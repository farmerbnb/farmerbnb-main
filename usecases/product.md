# Product Use Cases

## Seller creates new product
Given a user is a Seller,\
When the user submits information including product name, description, category, price, and unit,\
Then the platform should persist that information to the database.

### API
#### Create a Product
```
POST /products
```
##### Parameters
* name (required): Name of the product.
* category (required): Category of the product.
* description (optional): Description of the product. It's displayed when the buyers are viewing the product.
* image (optional): Image URL of the product.
* unit (required): The selling unit, e.g. 100 Gram or 1 Liter.
* price (required): The price of the product per unit.
* quantity (optional): The quantity of the product. The product will only be showned to buyers when the quantity is greater than 0.

##### Sample Request
```
POST /products
{
    name: "Japanese Kobe Beef",
    category: "meat",
    description: "Kobe Beef from Japan",
    unit: "100 gram",
    price: 20,
    quantity: 10
}
```
#### Sample Response
```
{
    data: {
        product: {      
            id: 123,
            name: "Japanese Kobe Beef",
            category: "meat",
            description: "Kobe Beef from Japan",
            unit: "100 gram",
            price: 20,
            quantity: 10
        }
    }
}
```
#### View a Product
```
GET /products/:product_id
```
##### Parameters
* product_id (required): ID of the product.

##### Sample Request
```
GET /products/123
```
#### Sample Response
```
{
    data: {
        product: {      
            id: 123,
            name: "Japanese Kobe Beef",
            category: "meat",
            description: "Kobe Beef from Japan",
            unit: "100 gram",
            price: 20,
            quantity: 10
        }
    }
}
```

## Seller manages their product
Given a product was created by a seller,\
When the seller submits the new product's attributes,\
The new product attributes should be updated in the database.

### API
#### Update a Product
```
PATCH /products/:product_id
```
##### Parameters
* product_id (required): ID of the product.
* name (optional): Name of the product.
* category (optional): Category of the product.
* description (optional): Description of the product. It's displayed when the buyers are viewing the product.
* image (optional): Image of the product.
* unit (optional): The selling unit, e.g. 100 Gram or 1 Liter.
* price (optional): The price of the product per unit.
* quantity (optional): The quantity of the product. The product will only be showned to buyers when the quantity is greater than 0.

##### Sample Request
```
PATCH /products/123
{  
    price: 25,
    quantity: 50
}
```
#### Sample Response
```
{
    data: {
        product: {      
            id: 123,
            name: "Japanese Kobe Beef",
            category: "meat",
            description: "Kobe Beef from Japan",
            unit: "100 gram",
            price: 25,
            quantity: 50
        }
    }
}
```

## User searches products
Given there are products in the database,\
When the user searches products,\
The platform should return a list of products that match with the search criteria.

### API
#### List Products
```
GET /products
```
##### Parameters
If no parameter is provided, this API will list all products.
* name (optional): Name to match with products.
* category (optional): Category to match with products.
* seller_id (optional): ID of the seller.
* price_max (optional): Maximum price of the product per unit.
* price_min (optional): Minimum price of the product per unit.

##### Sample Request
```
GET /products
{  
    category: "meat",
    price_max: 100
}
```
#### Sample Response
```
{
    data: {
        products: [
            {
                id: 123,
                name: "Japanese Kobe Beef",
                category: "meat",
                description: "Kobe Beef from Japan",
                unit: "100 gram",
                price: 25,
                quantity: 50
            },
            {
                id: 124,
                name: "Norwegian Salmon",
                category: "meat",
                description: "Salmon from Norway",
                unit: "50 gram",
                price: 30,
                quantity: 100
            },
        ]         
    }
}
```
