## Usecase

## payment

### Scenario 1: 

Given the buyer confirms the purchase order

AND the buyer has billing address and shipping address

AND the buyer on the checkout page

WHEN the buyer confirms the payment details and shipping details

AND the buyer performs  'checkout' action
 
THEN the system redirects the buyer to payment gateway

AND once payment has done, the system display the confirmation page with receipt attachment.

### API required:

*Get buyer billing & shipping address

	Get/buyerDetails
	{
		"buyerId": 123,
		
		"shipment address" : "Clementi Avenue 1",
		
		"shipment code" : 123456,
		
		"billing address" : "Clementi Avenue 1",
		
		"billing code": 123456
	}


*Get payment method

	Get/paymentMethod?buyerId=123
	{
		"buyerId": 123,
		"paymentMethod":":[
		{"method":"Cash"},
		{"method":"Visa","CardNo":123434566,"cvv":007,"expire_date":"12/2021","minimunpayment":50},
		{"method":"Master","cardNo":735534733,"cvv":007,"expire_date":"12/2021","minimunpayment":50}
		],
		"billing code": 123456
	}

*Post payment details to payment gateway

	POST / payment gateway API
	
	{
		"merchantCode":12323,
		"return_url":"https://farmerbnb.com/paymentcompleted",
		"token": {
        "type": "BILLING_AGREEMENT",
        "id": "B-6282561050045662U"
		}	
		"reference_id":12351250
		"amount": {
        "currency_code": "SGD",
        "value": "240.00"
		}
	}


*POST createReceipt

	{
		"reference_id":20550145
		"amount": {
        "currency_code": "SGD",
        "value": "240.00"
		}
		"buyerId": 123,
		"paymentMethod": "Visa",
		"paymentDate": "2020-03-26 08:00:00"
	}


### optional:

Calls third-party payment API directly. For example, Paypal
Paypal sandbox URL: https://developer.paypal.com/docs/checkout/

1. just add paypal checkout button in web page
2. when user clicks on button, it calls paypal checkout url
3. paypal will handle the payment (paypal account, credit card, debit card etc)
4. after receiving the response, display confirmation to user

Json request to paypal:
curl -v -X POST https://api.sandbox.paypal.com/v2/checkout/orders 

"Content-Type: application/json" 

"Authorization: Bearer Access-Token" 

 '{
  "intent": "CAPTURE",
  "application_context": {
    "return_url": "https://example.com/return",
    "cancel_url": "https://example.com/cancel",
    "client_configuration": {
      "integration_artifact": "PAYPAL_JS_SDK",
      "experience": {
        "user_experience_flow": "FULL_PAGE_REDIRECT",
        "entry_point": "PAY_WITH_PAYPAL",
        "channel": "WEB",
        "product_flow": "HERMES"
      }
    },
    "preferred_payment_source": {
      "token": {
        "type": "BILLING_AGREEMENT",
        "id": "B-6282561050045662U"
      }
    }
  },
  
  "purchase_units": [
    {
      "reference_id": "PUHF",
      "amount": {
        "currency_code": "USD",
        "value": "240.00"
      }
    }
  ]
}'


Json responses

{
  "id": "5O190127TN364715T",
  "status": "CREATED",
  "links": [
    {
      "href": "https://api.paypal.com/v2/checkout/orders/5O190127TN364715T",
      "rel": "self",
      "method": "GET"
    },
    {
      "href": "https://api.paypal.com/checkoutnow?token=5O190127TN364715T",
      "rel": "approve",
      "method": "GET"
    },
    {
      "href": "https://api.paypal.com/checkoutnow?token=5O190127TN364715T&exp_flow=authenticate",
      "rel": "authenticate",
      "method": "GET"
    },
    {
      "href": "https://api.paypal.com/v2/checkout/orders/5O190127TN364715T/capture",
      "rel": "capture",
      "method": "POST"
    }
  ]
}

