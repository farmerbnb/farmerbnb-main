# Fresh Farm Food

Architecting Scalable Systems - Practice Module Project

Project Proposal https://docs.google.com/document/d/1FB7u87D1kSPyN-jiqbXWIvYlvJLksybMInfkOsmvfNY

Kanban Board https://trello.com/b/E4qjQ3mC/farmer-bb

Google Drive https://drive.google.com/drive/u/0/folders/134nsdKYbn3yGayQCvR9i-QQB-w95X5mQ

Bounded Contexts https://app.diagrams.net/#G14hexupMQehFpB7W2M2yv6mvQSX6Td-45
